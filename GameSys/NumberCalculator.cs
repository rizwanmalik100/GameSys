﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameSys
{
    public class NumberCalculator : INumberCalculator
    {
        private const int Thirty = 30;
        private const int Squared = 2;
        private const double Half = 0.5d;
        private const int Ten = 10;
        private const int TwentyFive = 25;
        private const int Hundred = 100;
        private const int One = 1;
        private const int SeriesThird = 2;
        private const double Zero = 0.00d;


        public double CalCulateFirstNumber(double xValue)
        {
            var squared = Math.Pow(xValue, Squared);
            var thirtyX = Thirty*xValue;

            var total = squared* Half;
            total += thirtyX;
            total += Ten;
            total /= TwentyFive;

            return total;
        }

        public double CalCulateGrowthRate(double growValue, double firstNumber)
        {
            if (firstNumber == Zero)
            {
                throw new ArgumentException("firstNumber cannot be 0");
            }

            var growth = (growValue/Hundred)*Squared;
            growth /= TwentyFive;
            growth /= firstNumber;
            return growth;
        }

        public IEnumerable<double> CalculateSeries(int length, double growValue, double firstNumber)
        {
            if (length < 1)
            {
                throw new ArgumentException("Series must be > 0");
            }

            ISet <double> results = new HashSet<double>();

            results.Add(Round(firstNumber));

            int index = One;
            while (results.Count < length)
            {
                var value = Math.Pow(firstNumber, index);
                value *= growValue;
                results.Add(Round(value));
                ++index;
            }

            return results.OrderBy(i => i);
        }

        public Tuple<double,double> GetSeriesSpecialNumbers(IList<double> series, double yValue, double zValue)
        {
            if (series.Count < SeriesThird)
            {
                throw new ArgumentException("Series must have atleast 3 elements");
            }
            else if (zValue == Zero)
            {
                throw new ArgumentException("zValue cannot be 0");
            }

            var result = new Tuple<double,double>(series.OrderBy(i => i).ElementAt(SeriesThird), CalculateSpecialDifference(series, yValue,zValue));

            return result;
        }

        private double CalculateSpecialDifference(IList<double> series, double yValue, double zValue)
        {
            double approximateNumber = yValue / zValue;

            var difference = (from seriesValue in series
                             let value = Math.Abs(approximateNumber - seriesValue)
                             select new KeyValuePair<double, double>(seriesValue, value))
                             .OrderBy(i => i.Value)
                             .ThenByDescending(i => i.Key);
            
            return difference.First().Key;
        }

        private double Round(double number)
        {
            number *= 4;
            number = Math.Round(number, MidpointRounding.AwayFromZero);
            number /= 4;
            return number;
        }
    }
}
