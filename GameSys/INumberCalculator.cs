﻿using System;
using System.Collections.Generic;

namespace GameSys
{
    public interface INumberCalculator
    {
        double CalCulateFirstNumber(double xValue);
        double CalCulateGrowthRate(double growValue, double firstNumber);
        IEnumerable<double> CalculateSeries(int length, double growValue, double firstNumber);
        Tuple<double, double> GetSeriesSpecialNumbers(IList<double> series, double yValue, double zValue);
    }
}