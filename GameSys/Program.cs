﻿using System;
using System.Linq;

namespace GameSys
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                var numberCalculator = new NumberCalculator();
                var numberParser = new NumberParser();

                Console.WriteLine("Enter initial X");
                var initialValue = numberParser.ParseDouble(Console.ReadLine());
                var initialX = numberCalculator.CalCulateFirstNumber(initialValue);

                Console.WriteLine("When x={0}, firstNumber={1}",initialValue,initialX);

                Console.WriteLine("Enter growth rate");
                var initialGrowthValue = numberParser.ParseDouble(Console.ReadLine());
                var growthValue = numberCalculator.CalCulateGrowthRate(initialGrowthValue, initialX);
                Console.WriteLine("When y={0}, growthRate={1}", initialGrowthValue, growthValue);

                Console.WriteLine("Enter series size");
                var seriesSize = numberParser.ParseInt(Console.ReadLine());
                var series = numberCalculator.CalculateSeries(seriesSize, growthValue, initialX);

                foreach (var seriesItem in series)
                {
                    Console.WriteLine(seriesItem);
                }

                Console.WriteLine("Enter y value");
                var initialY = numberParser.ParseDouble(Console.ReadLine());

                Console.WriteLine("Enter z value");
                var initialZ = numberParser.ParseDouble(Console.ReadLine());

                var specialNumbers = numberCalculator.GetSeriesSpecialNumbers(series.ToList(), initialY, initialZ);
                Console.WriteLine("{0} is special",specialNumbers.Item1);
                Console.WriteLine("For y {0} and z {1} : {2} is special", initialY, initialZ, specialNumbers.Item1);

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }

            Console.WriteLine("Press enter to quit");
            Console.ReadLine();
        }
    }
}
