﻿using System;

namespace GameSys
{
    public class NumberParser : INumberParser
    {
        public double ParseDouble(String number)
        {
            double numberValue;
            if (!double.TryParse(number, out numberValue))
            {
                throw new ArgumentException(number +  " is not a valid double");
            }

            return numberValue;
        }

        public int ParseInt(String number)
        {
            int numberValue;
            if (!int.TryParse(number, out numberValue))
            {
                throw new ArgumentException(number + " is not a valid int");
            }

            return numberValue;
        }

    }
}
