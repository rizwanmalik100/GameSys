﻿using System;

namespace GameSys
{
    public interface INumberParser
    {
        double ParseDouble(String number);
        int ParseInt(String number);
    }
}