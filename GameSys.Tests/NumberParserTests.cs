﻿using System;
using NUnit.Framework;

namespace GameSys.Tests
{
    [TestFixture]
    class NumberParserTests
    {
        private INumberParser _numberParser;

        [SetUp]
        public void Init()
        {
            _numberParser = new NumberParser();
        }

        [Test]
        public void DoubleParsedCorrectly()
        {
            var result = _numberParser.ParseDouble("6.66");
            Assert.AreEqual(6.66,result);
        }

        [Test]
        public void DoubleCannotBeParsedException()
        {
            Assert.Throws<ArgumentException>(() => _numberParser.ParseDouble("1.de"));
        }

        [Test]
        public void IntParsedCorrectly()
        {
            var result = _numberParser.ParseInt("6");
            Assert.AreEqual(6, result);
        }

        [Test]
        public void IntCannotBeParsedException()
        {
            Assert.Throws<ArgumentException>(() => _numberParser.ParseDouble("1.de"));
        }
    }

}
