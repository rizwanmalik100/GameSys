﻿using System;
using System.Linq;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace GameSys.Tests
{
    [TestFixture]
    public class NumberCalculatorTests
    {
        private INumberCalculator _numberCalculator;

        [SetUp]
        public void Init()
        {
            _numberCalculator = new NumberCalculator();
        }

        [Test]
        public void XCalculatedCorrectly()
        {
            var result =_numberCalculator.CalCulateFirstNumber(1);
            Assert.AreEqual(1.6d,Math.Round(result,1));
        }

        [Test]
        public void XCalculatedZeroCorrectly()
        {
            var result = _numberCalculator.CalCulateFirstNumber(0);
            Assert.AreEqual(0.4d, Math.Round(result, 1));
        }

        [Test]
        public void XCalculatedNegativeCorrectly()
        {
            var result = _numberCalculator.CalCulateFirstNumber(-1);
            Assert.AreEqual(-0.8d, Math.Round(result, 1));
        }

        [Test]
        public void CalculatedGrowthCorrectly()
        {
            var growth = _numberCalculator.CalCulateGrowthRate(5062.5,1.6);
            Assert.AreEqual(2.5d,Math.Round(growth,1));
        }

        [Test]
        public void CalculatedZeroGrowthCorrectly()
        {
            Assert.Throws<ArgumentException>(() => _numberCalculator.CalCulateGrowthRate(0, 0));
        }

        [Test]
        public void CalculatedNegativeGrowthCorrectly()
        {
            var growth = _numberCalculator.CalCulateGrowthRate(-1, -1.6);
            Assert.AreEqual(0.0d, Math.Round(growth, 1));
        }

        [Test]
        public void SequenceHasCorrectNumElements()
        {
            var result =_numberCalculator.CalculateSeries(5, 2.5, 1.62);
            Assert.AreEqual(5,result.Count());
        }

        [Test]
        public void SequenceHasSingleElement()
        {
            var result = _numberCalculator.CalculateSeries(1, 2.5, 1.62);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void SequenceHasZeroElement()
        {
            Assert.Throws<ArgumentException>(() =>_numberCalculator.CalculateSeries(0, 2.5, 1.62));
        }

        [Test]
        public void SequenceHasCorrectFirstElement()
        {
            var result = _numberCalculator.CalculateSeries(5, 2.5, 1.62);
            Assert.AreEqual(1.5, result.First());
        }

        [Test]
        public void SequenceHasCorrectLastElement()
        {
            var result = _numberCalculator.CalculateSeries(5, 2.5, 1.62);
            Assert.AreEqual(17.25, result.Last());
        }

        [Test]
        public void SeriesReturnsCorrectSpecialFirst()
        {
            var series = new [] {1.5,4,6.5,10.75,17.25, 5.2 };
            var result = _numberCalculator.GetSeriesSpecialNumbers(series,1000,160);
            Assert.AreEqual(5.2, result.Item1);
        }

        [Test]
        public void SeriesReturnsCorrectSpecialSecond()
        {
            var series = new [] { 1.5, 4, 6.5, 10.75, 17.25 };
            var result = _numberCalculator.GetSeriesSpecialNumbers(series, 1000, 160);
            Assert.AreEqual(6.5, result.Item2);
        }

        [Test]
        public void SeriesReturnsCorrectDuplicateSpecialSecond()
        {
            var series = new [] { 1.5, 4, 6.5, 6.0, 10.75, 17.25 };
            var result = _numberCalculator.GetSeriesSpecialNumbers(series, 1000, 160);
            Assert.AreEqual(6.5, result.Item2);
        }

        [Test]
        public void SeriesReturnsCorrectZeroY()
        {
            var series = new [] { 1.5, 4, 6.5, 6.0, 10.75, 17.25 };
            var result = _numberCalculator.GetSeriesSpecialNumbers(series, 0, 160);
            Assert.AreEqual(1.5, result.Item2);
        }

        [Test]
        public void SeriesReturnsCorrectNegativeY()
        {
            var series = new [] { 1.5, 4, 6.5, 6.0, 10.75, 17.25 };
            var result = _numberCalculator.GetSeriesSpecialNumbers(series, -1, 160);
            Assert.AreEqual(1.5, result.Item2);
        }

        [Test]
        public void SeriesReturnsCorrectZeroZ()
        {
            var series = new[] { 1.5, 4, 6.5, 6.0, 10.75, 17.25 };
            Assert.Throws<ArgumentException>(() => _numberCalculator.GetSeriesSpecialNumbers(series, 1000, 0));
        }

        [Test]
        public void SeriesReturnsCorrectNegativeZ()
        {
            var series = new [] { 1.5, 4, 6.5, 6.0, 10.75, 17.25 };
            var result = _numberCalculator.GetSeriesSpecialNumbers(series, -1, -1);
            Assert.AreEqual(1.5, result.Item2);
        }
    }
}